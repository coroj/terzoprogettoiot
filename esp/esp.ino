#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>


/* wifi network name */
char* ssidName = "";
/* WPA2 PSK password */
char* pwd = "";
/* service IP address */ 
char* address = "http://f9e0c7b1.ngrok.io";

void setup() { 
  //Serial.begin(115200);
  Serial.begin(9600);                                
  WiFi.begin(ssidName, pwd);
 
  while (WiFi.status() != WL_CONNECTED) {  
    delay(500);
   
  } 
 
}

int sendData(String address, float value){  
   HTTPClient http;    
   http.begin(address + "/api/umidita");      
   http.addHeader("Content-Type", "application/json");     
   String msg = 
    String("{ \"HumidityValue\": ") + String(value) +"}";
   int retCode = http.POST(msg);   
   http.end();  
        
   return retCode;
}
   
void loop() { 
 if (WiFi.status()== WL_CONNECTED){   

   // valore da leggere 
   float rawValue = (float) analogRead(A0); /// 1023.0;
   float value = map(rawValue,0,1023,0,35);
   //float value = 26.0;
   int code = sendData(address, value);
   
 }
 
 delay(5000);  
 
}
