#ifndef __SONAR__
#define __SONAR__
#include "Arduino.h"
class Sonar { 
public:
  Sonar(int trigPin,int echoPin);
  float readDistance();
private:
  int trigPin;
  int echoPin; 
  const float vs = 331.5 + 0.6*20;
};


#endif
