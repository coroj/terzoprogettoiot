#ifndef __SHARED_STATE__
#define __SHARED_STATE__
#include "Arduino.h"
class SharedState {

private:
  enum {AUTO, MANUAL} dispenserMode;
  enum {B_OPEN, B_CLOSED} typeConnection;
  int quantity;
  int umidity;
  int lastUmidity;
  bool switched;
  String bMsg; //bluetooth message
  String sMsg; //serial message
  bool distanceOK;
  
public:
  
  SharedState() {
  this->umidity = 0;
  this->lastUmidity = umidity;
  this->quantity = 0;
	this->dispenserMode = AUTO;
  this->typeConnection = B_CLOSED;
	this->switched = false;
  this->distanceOK = false;
  this->bMsg = "0";
  this->sMsg = "0";
  }
  
  bool isAutoMode() { return this->dispenserMode == AUTO; }
  bool isManualMode() { return this->dispenserMode == MANUAL; }
  bool isSwitched() { return this->switched; }
  bool isConnectionOpen() { return this->typeConnection == B_OPEN; }
  bool isConnectionClosed() { return this->typeConnection == B_CLOSED; }
  bool isDistanceOK(){ return this->distanceOK;  }
  void setAutoMode() { this->dispenserMode = AUTO; }
  void setManualMode() { this->dispenserMode = MANUAL; }
  void setSwitch(bool s) { this->switched = s; }
  void setConnectionOpen() { this->typeConnection = B_OPEN; }
  void setConnectionClosed() { this->typeConnection = B_CLOSED; }
  
  void setQuantity(int qnt) { this->quantity = qnt; }
  void setUmidity(int u) {  umidity = u;  }
  void setLastUmidity(int u) {  lastUmidity = u;  }
  void setBMsg(String bMsg) { this->bMsg = bMsg; }
  void setSMsg(String sMsg) { this->sMsg = sMsg; }

  String getBMsg(){ return this->bMsg; }
  void setDistanceOK(bool value){ this->distanceOK = value; }
  int getQuantity() { return this->quantity; }
  int getUmidity() { return umidity; }
  int getLastUmidity() { return lastUmidity; }
};
#endif
