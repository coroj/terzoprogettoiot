#ifndef __BLUETOOTHTASK__
#define __BLUETOOTHTASK__
#include "Task.h"
#include "SharedState.h"
#include "Bluetooth.h"
#include "Arduino.h"
class BluetoothTask: public Task {
private:
  Bluetooth* bluetooth;
  SharedState* state;
public:
  BluetoothTask(SharedState* state);
  void init(int period);  
  void tick();
};
#endif
