#ifndef __BLUETOOTH__
#define __BLUETOOTH__
#include <SoftwareSerial.h>
#include <Wire.h>

class Bluetooth {
private:
  SoftwareSerial* channel; //ricordati di mettere le define
public:
  Bluetooth();
  SoftwareSerial* getChannel() { return this->channel; }
};
#endif
