#ifndef __DISPENSERTASK__
#define __DISPENSERTASK__

#include "Task.h"
#include "ServoTimer2.h"
#include "SharedState.h"
#include "Arduino.h"
class DispenserTask: public Task {
private:
  const int MAPPING_CONST = 180 / 15;
  int pin;
  ServoTimer2* servo;
  SharedState* state;
public:
  DispenserTask(SharedState* state, int pin);
  void init(int period);  
  void tick();
};
#endif
