#include "ThreeLedsTask.h"
#include "Arduino.h"

ThreeLedsTask::ThreeLedsTask(SharedState* state, int pin0, int pin1, int pin2){
  this->state = state;
  this->pin[0] = pin0;    
  this->pin[1] = pin1;    
  this->pin[2] = pin2;    
}
  
void ThreeLedsTask::init(int period){
  Task::init(period);
  for (int i = 0; i < 3; i++){
    led[i] = new Led(pin[i]); 
  }   
}
  
void ThreeLedsTask::tick() {
  
  if (state->isAutoMode()) {
    led[0]->switchOn();
    led[2]->switchOff();
  } else if(state->isManualMode()) {
    led[0]->switchOff();
    led[2]->switchOn();
  }
  
  int val = map(this->state->getQuantity(), 0, 15, 0, 255);
  led[1]->setIntensity(val);
  //led[1]->switchOn();
  //Serial.println(val);
}
