#include "BluetoothTask.h"

BluetoothTask::BluetoothTask(SharedState* state) {
	this->state = state;
	this->bluetooth = new Bluetooth();
}

void BluetoothTask::tick() {
  SoftwareSerial* channel = this->bluetooth->getChannel();
  if (channel->available()) {
		String msg="";
		while (channel->available()) { 
		  msg += (char) channel->read();
          
		}
    if (msg.equals("O")) { //OPEN CONNECTION.
      //Serial.println("ricevuto O da bluetooth");
      this->state->setManualMode();
      this->state->setSwitch(true);
      this->state->setConnectionOpen();
      this->state->setQuantity(0); // reset erogazione
    } else if(msg.equals("C")) { //NOT CONNECTION
     // Serial.println("ricevuto C da bluetooth");
      this->state->setAutoMode();
      this->state->setSwitch(true);
      this->state->setConnectionClosed();
      this->state->setQuantity(0); // reset erogazione
    }
    else{
    //  if(this->state->isDistanceOK()){ //bisogna essere abbastanza vicini per cambiare l' erogazione
	      //this->state->setBMsg(msg);
        this->state->setQuantity(msg.toInt());
     // }
    }
  }

  //invio l' umidita quando sono in mod.manuale
  if(this->state->isManualMode()){
    if(this->state->getLastUmidity() != this->state->getUmidity()){ //evitare polling
      this->state->setLastUmidity(this->state->getUmidity());
      channel->print(String(this->state->getUmidity()));
      //Serial.println(this->state->getUmidity());
    }  
  }
}

void BluetoothTask::init(int period) {
  Task::init(period);
}
