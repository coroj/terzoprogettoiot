#ifndef __SONARTASK__
#define __SONARTASK__

#include "Task.h"
#include "SharedState.h"
#include "Arduino.h"
#include "Sonar.h"
class SonarTask: public Task {
private:
  int trigPin;
  int echoPin;
  const float DIST = 0.3; 
  SharedState* state;
  Sonar* sonar;
public:
  SonarTask(SharedState* state, int trigPin, int echoPin);
  void init(int period);  
  void tick();
};
#endif
