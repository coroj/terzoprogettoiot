#ifndef __SERIALTASK__
#define __SERIALTASK__
#include "Arduino.h"
#include "Task.h"
#include "SharedState.h"
class SerialTask: public Task {
private:
  SharedState* state;
public:
  SerialTask(SharedState* state);
  void init(int period);  
  void tick();
};
#endif
