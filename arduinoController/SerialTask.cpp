#include "SerialTask.h"
SerialTask::SerialTask(SharedState* state){
  this->state = state;
}
  
void SerialTask::init(int period){
  Task::init(period);
  Serial.begin(9600);
}
  
void SerialTask::tick(){
    if (Serial.available() > 0) { //riceve da server java
  		String msg="";
  		while (Serial.available() > 0) { 
  		  msg += (char) Serial.read();
        delayMicroseconds(3);        
  		}
     //Serial.println("R:"+msg);
  	this->state->setSMsg(msg);

    if(this->state->isAutoMode()){
      this->state->setQuantity(msg.toInt());
    }
    else{   
      this->state->setUmidity(msg.toInt());
      //Serial.println(this->state->getUmidity());
    }

  }
  if (this->state->isSwitched()) {
      if (this->state->isConnectionOpen()) {
        Serial.write("O");
      } else {
        Serial.write("C");
      }
      this->state->setSwitch(false);
    }
}
