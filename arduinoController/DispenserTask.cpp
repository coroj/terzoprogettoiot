#include "DispenserTask.h"
DispenserTask::DispenserTask(SharedState* state, int pin){
  this->pin = pin;
  this->state = state;
  servo = new ServoTimer2();
}
  
void DispenserTask::init(int period){
  Task::init(period);
  servo->attach(pin);
}
  
void DispenserTask::tick(){
  int val;

  val = this->state->getQuantity();

  val = map(val, 0, 15, 550, 2250);
  
  servo->write(val);
}
