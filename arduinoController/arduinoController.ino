#include "Scheduler.h"
#include "ThreeLedsTask.h"
#include "DispenserTask.h"
#include "SonarTask.h"
#include "SharedState.h"
#include "BluetoothTask.h"
#include "SerialTask.h"
Scheduler sched;
SharedState* state;
void setup(){

  
  sched.init(50);
  state = new SharedState();
  Task* t0 = new DispenserTask(state, 11);
  t0->init(150);
  sched.addTask(t0);
  Task* t1 = new ThreeLedsTask(state,4,5,7);
  t1->init(50);
  sched.addTask(t1);
  Task* t2 = new SonarTask(state,12,13);
  t2->init(1000);
  //t2->init(500);
  sched.addTask(t2);
  Task* t3 = new BluetoothTask(state);
  t3->init(400);
  //t3->init(600);
  sched.addTask(t3);
  Task* t4 = new SerialTask(state);
  t4->init(700);
  //t4->init(300);
  sched.addTask(t4);
  
  /*
  Task* t0 = new BlinkTask(10);
  t0->init(500);
  sched.addTask(t0);

  Task* t1 = new ThreeLedsTask(7,8,9);
  t1->init(150);
  sched.addTask(t1);  
  */
}

void loop(){
  
  sched.schedule();
}
