#include "SonarTask.h"
SonarTask::SonarTask(SharedState* state, int trigPin, int echoPin) {
	this->state = state;
	this->trigPin = trigPin;
	this->echoPin = echoPin;
	this->sonar = new Sonar(trigPin, echoPin);
}

void SonarTask::tick() {
  //Serial.println(this->sonar->readDistance());
  this->state->setDistanceOK(sonar->readDistance() <= this->DIST);
}

void SonarTask::init(int period) {
  Task::init(period);
  Serial.begin(9600);
}
