#ifndef __THREELEDSTASK__
#define __THREELEDSTASK__

#include "Task.h"
#include "Led.h"
#include "SharedState.h"
class ThreeLedsTask: public Task {
  SharedState* state;
  int pin[3];
  Light* led[3];

public:

  ThreeLedsTask(SharedState* state, int pin0, int pin1, int pin2);  
  void init(int period);  
  void tick();
};

#endif
