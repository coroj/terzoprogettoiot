<?php
define("HOST", "localhost"); // E' il server a cui ti vuoi connettere.
define("USER", "progetto3iot"); // E' l'utente con cui ti collegherai al DB.
define("PASSWORD", "progetto3iot"); // Password di accesso al DB.
define("DATABASE", "progetto3iot"); // Nome del database.
$conn = new mysqli(HOST, USER, PASSWORD, DATABASE);
// Se ti stai connettendo usando il protocollo TCP/IP, invece di usare un socket UNIX, ricordati di aggiungere il parametro corrispondente al numero di porta.
?>
