$(document).ready(function(){
  // Load the Visualization API and the piechart package.
  google.charts.load('current', {'packages':['corechart']});
  google.charts.load('current', {'packages':['table']});

  // Set a callback to run when the Google Visualization API is loaded.
  google.charts.setOnLoadCallback(drawCharts);

});

function drawCharts() {
  getInfoSerra();
  drawUmiditaChart();
  drawIrrigazioneChart();
  drawSegnalazioneTable();

}

function getInfoSerra(){
  var jsonData;
   $.ajax({

      type:"GET",
      // bisognerebbe fare crossdomainurl: "http://localhost:8080/api/serraInfo",
      url: "getInfoSerra.php",
      data: {
        address: 'http://localhost:8080/api/serraInfo'
      },
      dataType: "json",

      success: function (response){

        var valoreUmiditaDiv = $("#valore_umidita_div");
        var modalitaDiv = $("#modalita_div");
        var quantitaErogataDiv = $("#quantita_erogata_div");

        valoreUmiditaDiv.text("Valore umidita' : " + response.HumidityValue);
        modalitaDiv.text("Modalita' : " + response.Mode);
        quantitaErogataDiv.text("Quantita' attualmente erogata : "+response.ErogatedLitres);








//chart.draw(data, options);
      },

      error:function (xhr, ajaxOptions, thrownError)
      {
        alert("ERROR code:"+xhr.status+" "+thrownError);
      },
      complete: function(){
        setTimeout(getInfoSerra,3000);
      }

      //  setTimeout(jsonData,3000);

      });
}


function drawSegnalazioneTable(){
  var jsonData;
   $.ajax({
     type:"POST",
     url: "http://localhost/frontendHTML/getData.php",
      //url: "getData.php",
      dataType: "text",
      data: { request: "storico_segnalazioni"},
      success: function (response){

        // Create our data table out of JSON data loaded from server.
        //var array  = JSON.parse(jsonString);

        var data = new google.visualization.DataTable();
        data.addColumn('number', 'Id segnalazione');
        data.addColumn('string', "Descrizione segnalazione");
        data.addColumn('string', "Timestamp");


        var parsed = JSON.parse(response);

        data.addRows(parsed);

        var options = {

          legend: { position: "none" },
          page: 'enable',
          pageSize: 5

        };


        var chart = new google.visualization.Table(document.getElementById('table_segnalazioni_div'));
        chart.draw(data, options);





//chart.draw(data, options);
      },
      error:function (xhr, ajaxOptions, thrownError)
      {
        alert(thrownError);
      },
      complete: function(){
        setTimeout(drawSegnalazioneTable,3000);
      }

      //  setTimeout(jsonData,3000);

      });
}

function drawIrrigazioneChart(){
  var jsonData;
   $.ajax({
     type:"POST",
     url: "http://localhost/frontendHTML/getData.php",
    //  url: "getData.php",
      dataType: "text",
      data: { request: "storico_irrigazioni"},
      success: function (response){

        // Create our data table out of JSON data loaded from server.
        //var array  = JSON.parse(jsonString);

        var data = new google.visualization.DataTable();
        data.addColumn('number', 'Id irrigazione');
        data.addColumn('number', "Durata irrigazione");
        data.addColumn('number', "Totale erogato");
        //data.addColumn('number', "prova");
        var parsed = JSON.parse(response);

        data.addRows(parsed);

        var options = {

          hAxis: {

            title: 'id erogazioni',
            format: '0'

          },
          vAxis: {

            title: "Dettagli irrigazioni"

          },

          legend: { position: "none" }
        };


        var chart = new google.visualization.ColumnChart(document.getElementById('chart_irrigazioni_div'));

        chart.draw(data, options);





//chart.draw(data, options);
      },
      error:function (xhr, ajaxOptions, thrownError)
      {
        alert(thrownError);
      },
      complete: function(){
        setTimeout(drawIrrigazioneChart,3000);
      }

      //  setTimeout(jsonData,3000);

      });
}


function drawUmiditaChart(){
  var jsonData;
   $.ajax({
     type:"POST",
     url: "http://localhost/frontendHTML/getData.php",
      //url: "getData.php",
      dataType: "text",
      data: { request: "storico_umidita"},
      success: function (response){

        // Create our data table out of JSON data loaded from server.
        //var array  = JSON.parse(jsonString);

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Ora rilevazione');
        data.addColumn('number', "Umidita' rilevata");
        //data.addColumn('number', "prova");
        var parsed = JSON.parse(response);

        data.addRows(parsed);

        var options = {

          hAxis: {

            title: 'Tempo'
          },
          vAxis: {

            title: "umidita' rilevata",
            viewWindow: {
             //max: 8000,
             min: 0
            }

          },

          legend: { position: "none" }
        };


        var chart = new google.visualization.ColumnChart(document.getElementById('chart_umidita_div'));

        chart.draw(data, options);





//chart.draw(data, options);
      },
      error:function (xhr, ajaxOptions, thrownError)
      {
        alert(thrownError);
      },
      complete: function(){
        setTimeout(drawUmiditaChart,3000);
      }

      //  setTimeout(jsonData,3000);

      });
}
