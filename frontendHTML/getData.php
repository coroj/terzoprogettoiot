<?php
include ('dbConnect.php');
// This is just an example of reading server side data and sending it to the client.
// It reads a json formatted text file and outputs it.

//$string = file_get_contents("sampleData.json");
if(isset($_POST['request'])){

    if($_POST['request'] == "storico_segnalazioni"){
      $query = "SELECT id,descrizione,timestamp FROM (
          SELECT * FROM storico_segnalazioni ORDER BY id DESC LIMIT 50
      ) sub
      ORDER BY id ASC";
      $first = true;
      if($result = $conn->query($query))
      {
          $string="[";

          if ($result->num_rows > 0) {
              while($row = mysqli_fetch_assoc($result)){

                  if($first){
                    $first = false;
                  }
                  else{
                    $string = $string.",";
                  }

                  $id = $row['id'];
                  $descrizione = $row['descrizione'];
                  $timestamp = date_create($row['timestamp']);
                  $timestamp_formattato = date_format($timestamp, 'd/m/Y H:i:s');
                  //$timestamp = date_create($row['timestamp']);
                  //$timestamp_formattato = date_format($timestamp, 'd/m/Y H:i:s');
                //  $string = $string.'['.$row["id"].',"'.$timestamp_formattato.'",'.$row["percentuale_umidita"].']';
                   $string = $string.'['.$id.',"'.$descrizione.'","'.$timestamp_formattato.'"]';
              }
          }

          $string = $string."]";
      }


      echo $string;
    }


    if($_POST['request'] == "storico_irrigazioni"){
      $query = "SELECT id,litri_portata,TIMESTAMPDIFF(SECOND,timestamp_inizio,timestamp_fine) AS durata FROM (
          SELECT * FROM storico_irrigazioni ORDER BY id DESC LIMIT 50
      ) sub
      ORDER BY id ASC";
      $first = true;
      if($result = $conn->query($query))
      {
          $string="[";

          if ($result->num_rows > 0) {
              while($row = mysqli_fetch_assoc($result)){

                  if($first){
                    $first = false;
                  }
                  else{
                    $string = $string.",";
                  }

                  $id = $row['id'];
                  $durata = $row['durata'];
                  $portata_erogata = $row['litri_portata'];
                  //$timestamp = date_create($row['timestamp']);
                  //$timestamp_formattato = date_format($timestamp, 'd/m/Y H:i:s');
                //  $string = $string.'['.$row["id"].',"'.$timestamp_formattato.'",'.$row["percentuale_umidita"].']';
                   $string = $string.'['.$id.','.$durata.','.$portata_erogata.']';
              }
          }

          $string = $string."]";
      }


      echo $string;
    }


    if($_POST['request'] == "storico_umidita"){
      $query = "SELECT timestamp,percentuale_umidita FROM (
          SELECT * FROM storico_livello_umidita ORDER BY id DESC LIMIT 50
      ) sub
      ORDER BY id ASC";
      $first = true;
      if($result = $conn->query($query))
      {
          $string="[";

          if ($result->num_rows > 0) {
              while($row = mysqli_fetch_assoc($result)){

                  if($first){
                    $first = false;
                  }
                  else{
                    $string = $string.",";
                  }

                  $timestamp = date_create($row['timestamp']);
                  $timestamp_formattato = date_format($timestamp, 'd/m/Y H:i:s');
                //  $string = $string.'['.$row["id"].',"'.$timestamp_formattato.'",'.$row["percentuale_umidita"].']';
                   $string = $string.'["'.$timestamp_formattato.'",'.$row["percentuale_umidita"].']';
              }
          }

          $string = $string."]";
      }


      echo $string;
  }
}
// Instead you can query your database and parse into JSON etc etc

?>
