package com.universita.serverJavaSmartGreenHouseManagement;

public class Constants {
	public static final String ARDUINO_PORT = "COM4";
	public static final int ARDUINO_BAUDRATE = 9600;
	
	public static final float DIST = 0.3f;
	
	public static final int UOTTIMALE = 30;
	
	public static final int U10 = 10;
	
	public static final int U20 = 20;
	
	public static final int U30 = 30;
	
	public static final int DELTAU = 5;
	
	public static final int PZERO = 0;
	
	public static final int PMIN = 5;
	
	public static final int PMED = 10;
	
	public static final int PMAX = 15;
	
	public static final int TMAX_EROGAZIONE = 20;
	
	public static final int MOLTIPLICATORE_MILLIS = 1000;
	
	public final static String BLUETOOTH_OPENED_CONNECTION = "O";
	    
	public static final String BLUETOOTH_CLOSED_CONNECTION = "C";
	
}
