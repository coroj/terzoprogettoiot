package com.universita.serverJavaSmartGreenHouseManagement.patternObserver;

import com.universita.serverJavaSmartGreenHouseManagement.common.Event;
import com.universita.serverJavaSmartGreenHouseManagement.common.Observer;

public interface Observable {
		
	void notifyEventToObservers(Event ev);

	public void addObserver(Observer obs);

	public void removeObserver(Observer obs);
}
