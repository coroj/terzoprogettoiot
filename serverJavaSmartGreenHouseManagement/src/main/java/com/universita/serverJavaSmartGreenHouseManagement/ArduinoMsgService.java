package com.universita.serverJavaSmartGreenHouseManagement;

import com.universita.serverJavaSmartGreenHouseManagement.commChannel.CommChannel;
import com.universita.serverJavaSmartGreenHouseManagement.commChannel.ExtendedSerialCommChannel;
import com.universita.serverJavaSmartGreenHouseManagement.common.Observable;
import com.universita.serverJavaSmartGreenHouseManagement.events.ArduinoMsgEvent;


public class ArduinoMsgService extends Observable {

	private CommChannel channel;
	private String port;
	private int rate;
	
	public ArduinoMsgService(String port, int rate){
		this.port = port;
		this.rate = rate;
	}
	
	void init(){
		try {
			channel = new ExtendedSerialCommChannel(port, rate);	
			// channel = new SerialCommChannel(port, rate);	
			System.out.println("Waiting Arduino for rebooting...");		
			Thread.sleep(4000);
			System.out.println("Ready.");		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		new Thread(() -> {
			while (true) {
				try {
					//System.out.println("waiting for arduino message");
					String msg = channel.receiveMsg();
					System.out.println("received "+msg);
					this.notifyEvent(new ArduinoMsgEvent(msg));
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}).start();
	}
	
	public void sendMsg(String msg) {
		channel.sendMsg(msg);
		System.out.println("sent "+msg);
	}
	
}
