package com.universita.serverJavaSmartGreenHouseManagement.common;

public interface Handler {
	void handle(Event ev);
}
