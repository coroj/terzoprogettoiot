package com.universita.serverJavaSmartGreenHouseManagement.common;

public interface Observer {

	boolean notifyEvent(Event ev);
}
