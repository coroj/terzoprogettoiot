package com.universita.serverJavaSmartGreenHouseManagement.management;

import com.universita.serverJavaSmartGreenHouseManagement.Constants;
import com.universita.serverJavaSmartGreenHouseManagement.state.ErogazioneEnum;
import com.universita.serverJavaSmartGreenHouseManagement.state.ModeEnum;

public class GreenHouseStateMachine {
	
	private static GreenHouseStateMachine greenHouseStateMachine = null;
	
	private ModeEnum actualMode;
	
	private ErogazioneEnum actualErogazione;
	
	private GreenHouseStateMachine() {
		
		startAutomaticMode();
		
	}
	
	public static GreenHouseStateMachine getStateMachine() {
		
		if(greenHouseStateMachine == null) {
			
			greenHouseStateMachine = new GreenHouseStateMachine();
			
		}
		
		return greenHouseStateMachine;
	}
	
	///////////////////////////////////////////////////////
	
	public void stopAutomaticMode() {
		
		setManualMode();
		
		setErogazioneZero();
		
	}
	
	public void startAutomaticMode() {
		
		setAutomaticMode();
		setErogazioneZero();
		
	}
	
	
	///////////////////////////////////////////////////////
	
	public void setErogazioneZero() {
		
		actualErogazione = ErogazioneEnum.ZERO;
		
	}
	
	public void setErogazioneMin() {
		
		actualErogazione = ErogazioneEnum.MIN;
		
	}
	
	public void setErogazioneMed() {
		
		actualErogazione = ErogazioneEnum.MED;
		
	}
	
	public void setErogazioneMax() {
		
		actualErogazione = ErogazioneEnum.MAX;
		
	}
	
	///////////////////////////////////////////////////
	
	public void setManualMode() {
		
		actualMode = ModeEnum.MANUAL;
		
	}
	
	public void setAutomaticMode() {
		
		actualMode = ModeEnum.AUTO;
		
	}
	
	///////////////////////////////////////////////////
	
	public ModeEnum getActualMode() {
		
		return this.actualMode;
		
	}
	
	public ErogazioneEnum getActualErogazione() {
	
		return this.actualErogazione;
	}
	
	/////////////////////////////////////////////////////
	public int getActualErogazioneValue() {
		
		return getErogazioneValueByEnum(getActualErogazione());
		
	}
	
	private int getErogazioneValueByEnum(ErogazioneEnum e) {
		int v = 0;
		if(e.equals(ErogazioneEnum.ZERO)) {
			v = Constants.PZERO;
		}
		
		else if(e.equals(ErogazioneEnum.MIN)) {
			v = Constants.PMIN;
		}
		
		else if(e.equals(ErogazioneEnum.MED)) {
			v = Constants.PMED;
		}
		
		else if(e.equals(ErogazioneEnum.MAX)) {
			v = Constants.PMAX;
		}
		
		return v;
	}
	
	public void processHumidityValue(float value) {
			
			if(value < Constants.U10) {
				setErogazioneMax();
			}
			else if(value >= Constants.U10 && value <= Constants.U20) {
				setErogazioneMed();
			}
			else if(value > Constants.U20 && value <= (Constants.U30 + Constants.DELTAU)) {
				setErogazioneMin();
			}
			else {
				setErogazioneZero();
			}
			
	}
	
}

