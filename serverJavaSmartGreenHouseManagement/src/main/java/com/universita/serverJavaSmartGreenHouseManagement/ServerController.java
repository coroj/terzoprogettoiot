package com.universita.serverJavaSmartGreenHouseManagement;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.universita.serverJavaSmartGreenHouseManagement.common.*;
import com.universita.serverJavaSmartGreenHouseManagement.devices.*;
import com.universita.serverJavaSmartGreenHouseManagement.events.HumidityESPEvent;
import com.universita.serverJavaSmartGreenHouseManagement.events.IrrigationCompletedEvent;
import com.universita.serverJavaSmartGreenHouseManagement.events.RESTInfoRequest;
import com.universita.serverJavaSmartGreenHouseManagement.events.RESTInfoResponse;
import com.universita.serverJavaSmartGreenHouseManagement.events.ArduinoMsgEvent;
import com.universita.serverJavaSmartGreenHouseManagement.management.GreenHouseStateMachine;
import com.universita.serverJavaSmartGreenHouseManagement.patternObserver.Observable;
import com.universita.serverJavaSmartGreenHouseREST.DataService;
import com.universita.serverJavaSmartGreenHouseREST.Utils;
import com.universita.serverJavaSmartGreenHouseManagement.state.*;
import com.universita.serverJavaSmartGreenHouseREST.model.SerraInfo;

public class ServerController extends BasicEventLoopController implements Observable {
	
	private static ServerController serverController = null;
	
	private ArduinoMsgService arduinoMsgService;
	private ObservableTimer timer;

	private GreenHouseStateMachine stateMachine;
	private ErogazioneEnum previousErogazione;
	private float umiditaAttuale;
	private float totaleErogato;
	private long lastIrrigazioneID;
	private boolean stillErogating;
	private List<Observer> observers;
	
	public ServerController(String port, int rate){
		
		//observer per notificare dati ricevuti da webservice per esp
		
		
		observers = new LinkedList<>();
		stateMachine = GreenHouseStateMachine.getStateMachine();
		previousErogazione = stateMachine.getActualErogazione();
		this.arduinoMsgService = new ArduinoMsgService(port, rate);
		arduinoMsgService.init();
		this.timer = new ObservableTimer();
		timer.addObserver(this);
		arduinoMsgService.addObserver(this); //per segnalare la ricezione di messaggi da arduino
		
		umiditaAttuale = -1;
	}
	
	public static ServerController getServerController() {
		
		if(serverController == null) {
			serverController = new ServerController(Constants.ARDUINO_PORT,Constants.ARDUINO_BAUDRATE);
			
			DataService.getDataService().addObserver(serverController); // per ricevere i messaggi da esp
			
		}
		
		return serverController;
		
	}
	
	
	protected void processEvent(Event ev){
		
				
				if(ev instanceof Tick) { //every second
					
					totaleErogato += stateMachine.getActualErogazioneValue();
					
				}
			
			
				if (ev instanceof EndTick){//after 5 seconds 
					if(stillErogating) {
						
						stillErogating = false;
					
						timer.stop();
						totaleErogato -= stateMachine.getActualErogazioneValue(); //ne conta uno in piu
						if(stateMachine.getActualErogazione() != ErogazioneEnum.ZERO) {
							//ancora stava erogando,c'è qualche problema, entra qui solo in caso di endtick
							
							try {
								Utils.insertSegnalazione(Utils.generateTimeoutErrorMessage(totaleErogato, umiditaAttuale));
							} catch (SQLException e) {
								e.printStackTrace();
							}			
							stateMachine.setErogazioneZero();
							arduinoMsgService.sendMsg(String.valueOf(stateMachine.getActualErogazioneValue())); //forzo a zero
							previousErogazione = stateMachine.getActualErogazione();
							
							
						}
						
						try {
							Utils.updateEndIrrigazione(lastIrrigazioneID, totaleErogato);
						} catch (SQLException e) {
							
							e.printStackTrace();
						}
						System.out.println("totale erogato"+totaleErogato);
					}
					
					
					
				}
				
				if(ev instanceof IrrigationCompletedEvent) {
					if(stillErogating) {
						
						stillErogating = false;
					
						timer.stop();
						
						try {
							Utils.updateEndIrrigazione(lastIrrigazioneID, totaleErogato);
						} catch (SQLException e) {
							
							e.printStackTrace();
						}
						System.out.println("totale erogato"+totaleErogato);
					}
				}
				
				
				if(ev instanceof HumidityESPEvent) {
					
					HumidityESPEvent event = (HumidityESPEvent)ev;
					umiditaAttuale = event.getHumidityValue();
					
					
					
					if(stateMachine.getActualMode() == ModeEnum.AUTO) {
					    stateMachine.processHumidityValue(umiditaAttuale);
						manageErogazioneAuto();
					}
					else {
						arduinoMsgService.sendMsg(String.valueOf(umiditaAttuale));
						//invia ad arduino l' umidita attuale -> solo in mod.manuale
					}
					
					
				
					
					
					
					
			   }
				
		// arduino events
		
				if(ev instanceof ArduinoMsgEvent) {
						  
					String msg = ((ArduinoMsgEvent)ev).getMsg();
						  //gestire connessione e disconnessione
						  if(stateMachine.getActualMode() == ModeEnum.AUTO && msg.equals(Constants.BLUETOOTH_OPENED_CONNECTION)) {
							  //la interrompo in modo safe, erog. settata a 0 e mode passa a manual
							  stateMachine.stopAutomaticMode();
							  
							  if(stillErogating) {
								  System.out.println("Interrompo l' irrigazione automatica");
								  //faccio fermare l' erogazione al punto attuale
								 // questo lo fa gia in arduino -> arduinoMsgService.sendMsg(String.valueOf(stateMachine.getActualErogazioneValue())); //zero
								  this.notifyEvent(new IrrigationCompletedEvent());
								
							  }
							  
							  System.out.println("Passa a modalita manuale");
								 
								  
								  
							  
						  }
						  else {
							  //se entro qui sono in manual mode
							  if(stateMachine.getActualMode() == ModeEnum.MANUAL && msg.equals(Constants.BLUETOOTH_CLOSED_CONNECTION)) {
							  
								  stateMachine.startAutomaticMode();
								  arduinoMsgService.sendMsg(String.valueOf(stateMachine.getActualErogazioneValue())); //zero
								  System.out.println("Passa a modalita automatica");
							  }
						  }
						  
		}
				
				
		// rest frontend request
				
		if(ev instanceof RESTInfoRequest) {
			
			int quantitaErogataAttualmente = stateMachine.getActualMode() == ModeEnum.AUTO ? stateMachine.getActualErogazioneValue() : -1;
			SerraInfo info = new SerraInfo(stateMachine.getActualMode().name(),
										   umiditaAttuale,
										   quantitaErogataAttualmente);
			
			this.notifyEventToObservers(new RESTInfoResponse(info));
			
			
		}
		
	}
	
	private void manageErogazioneAuto() {
		
		if(stateMachine.getActualErogazione() != previousErogazione) {//controllo per evitare di inviare dati semanticamente uguali
			if(previousErogazione == ErogazioneEnum.ZERO) {
				// all' inizio parto da spento, faccio partire timer e cose varie
				initializeIrrigazione();
			}
			previousErogazione = stateMachine.getActualErogazione();
		
			//se l' erogazione si completa entro i 5 secondi viene mandata erogazione 0 in questo flusso
			//altrimenti dopo 5s bisogna forzare la chiusura
			System.out.println("Invio ad arduino questo valore:"+ stateMachine.getActualErogazioneValue());
			arduinoMsgService.sendMsg(String.valueOf(stateMachine.getActualErogazioneValue()));
			
			
			if(previousErogazione == ErogazioneEnum.ZERO) {
				//quello attuale è 0, significa che sono passato da uno stato > 0 a 0
				// in quanto da 0 non posso andare a 0
				this.notifyEvent(new IrrigationCompletedEvent());
			}
			
		}
		
	}

	private void initializeIrrigazione() {
		try {
			
			lastIrrigazioneID = Utils.insertInizioIrrigazione();
			totaleErogato = 0;
			timer.scheduleTick(Constants.TMAX_EROGAZIONE * Constants.MOLTIPLICATORE_MILLIS); //per tick finale
			timer.start(Constants.MOLTIPLICATORE_MILLIS);
			stillErogating = true;
		} catch (SQLException e) {
			e.printStackTrace();
			stateMachine.setErogazioneZero();
			previousErogazione = stateMachine.getActualErogazione();
			
		}
	}

	@Override
	public void notifyEventToObservers(Event ev){
		synchronized (observers){
			for (Observer obs: observers){
				obs.notifyEvent(ev);
			}
		}
	}
	
	@Override
	public void addObserver(Observer obs){
		synchronized (observers){
			observers.add(obs);
		}
	}

	@Override
	public void removeObserver(Observer obs){
		synchronized (observers){
			observers.remove(obs);
		}
	}
}



