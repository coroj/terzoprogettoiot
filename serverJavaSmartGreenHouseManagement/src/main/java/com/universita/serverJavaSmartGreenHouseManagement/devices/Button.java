package com.universita.serverJavaSmartGreenHouseManagement.devices;

public interface Button {
	
	boolean isPressed();

}
