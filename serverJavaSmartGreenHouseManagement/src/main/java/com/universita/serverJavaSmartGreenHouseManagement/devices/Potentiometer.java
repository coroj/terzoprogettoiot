package com.universita.serverJavaSmartGreenHouseManagement.devices;

public interface Potentiometer {
	
	int getValue();

}
