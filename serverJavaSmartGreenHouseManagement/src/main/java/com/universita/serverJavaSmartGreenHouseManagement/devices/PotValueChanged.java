package com.universita.serverJavaSmartGreenHouseManagement.devices;

import com.universita.serverJavaSmartGreenHouseManagement.common.*;

public class PotValueChanged implements Event {
	private ObservablePotentiometer source;
	private int value;
	
	public PotValueChanged(ObservablePotentiometer source, int value){
		this.source = source;
		this.value = value;
	}
	
	public int getValue(){
		return value;
	}
	
	public ObservablePotentiometer getSource(){
		return source;
	}
}
