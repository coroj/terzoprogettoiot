package com.universita.serverJavaSmartGreenHouseManagement.devices;

public interface MotionDetectorSensor {
	boolean detected();
}
