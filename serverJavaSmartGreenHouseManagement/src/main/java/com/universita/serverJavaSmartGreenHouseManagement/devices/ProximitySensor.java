package com.universita.serverJavaSmartGreenHouseManagement.devices;

public interface ProximitySensor {

	boolean isObjDetected();
	
	double getObjDistance();
	
}
