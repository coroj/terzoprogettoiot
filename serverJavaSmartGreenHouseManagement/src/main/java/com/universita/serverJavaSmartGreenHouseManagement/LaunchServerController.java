package com.universita.serverJavaSmartGreenHouseManagement;
import com.universita.serverJavaSmartGreenHouseManagement.ServerController;
/**
 * In questo esempio un agente basato su architettura di controllo event-loop
 * (pattern reactor) invia "ping" e riceve dei "pong" via seriale (UART o Bluetooth)
 * aspettando 500 ms ad ogni ricezione.
 * 
 * Highlights:
 * - come usare un arch di controllo event-loop con message service, usando
 *   in un approccio ad eventi, senza primitive bloccanti.
 *   eliminare questa descrizione
 * @author Jacopo Corina
 *
 */
public class LaunchServerController {
	public static void main(String[] args) {
		
		ServerController serverController = ServerController.getServerController();
		serverController.start();
		
	}

}
