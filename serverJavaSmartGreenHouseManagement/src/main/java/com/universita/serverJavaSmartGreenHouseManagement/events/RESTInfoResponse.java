package com.universita.serverJavaSmartGreenHouseManagement.events;

import com.universita.serverJavaSmartGreenHouseManagement.common.Event;
import com.universita.serverJavaSmartGreenHouseREST.model.SerraInfo;

public class RESTInfoResponse implements Event {
	
	private SerraInfo serraInfo;
	
	public RESTInfoResponse(SerraInfo s) {
		
		this.serraInfo = s;
		
	}
	
	public SerraInfo getSerraInfo() {
		
		return this.serraInfo;
		
	}

}
