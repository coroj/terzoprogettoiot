package com.universita.serverJavaSmartGreenHouseManagement.events;

import com.universita.serverJavaSmartGreenHouseManagement.common.*;

public class ArduinoMsgEvent implements Event {
	
	private String msg;
	
	public ArduinoMsgEvent(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}
}
