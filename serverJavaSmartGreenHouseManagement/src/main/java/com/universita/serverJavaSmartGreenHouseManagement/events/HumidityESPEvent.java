package com.universita.serverJavaSmartGreenHouseManagement.events;

import com.universita.serverJavaSmartGreenHouseManagement.common.Event;

public class HumidityESPEvent implements Event {

		private float value;
		
		public HumidityESPEvent(float value){
			this.value = value;
		}

		public float getHumidityValue(){
			return value;
		}
		
}

