package com.universita.serverJavaSmartGreenHouseREST;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.universita.serverJavaSmartGreenHouseManagement.Constants;
import com.universita.serverJavaSmartGreenHouseREST.RESTConstants;
import com.universita.serverJavaSmartGreenHouseREST.model.DataPoint;


public class Utils {

	public static boolean insertHumidityValue(DataPoint dp) throws SQLException {
		  Connection conn = null;
	      QueryRunner queryRunner = new QueryRunner();
	      
	      //Step 1: Register JDBC driver
	      DbUtils.loadDriver(RESTConstants.JDBC_DRIVER);

	      //Step 2: Open a connection
	      System.out.println("Connecting to database...");
	  
		  conn = DriverManager.getConnection(RESTConstants.DB_URL, RESTConstants.USER, RESTConstants.PASS);
	  
		  int insertedRecords = queryRunner.update(conn, 
				            "INSERT INTO storico_livello_umidita(percentuale_umidita)  VALUES (?)",
				             dp.getValue());
						
		 DbUtils.close(conn);
		
	     
		 return insertedRecords > 0;
	}
	
	public static boolean insertSegnalazione(String string) throws SQLException {
		  Connection conn = null;
	      QueryRunner queryRunner = new QueryRunner();
	      
	      //Step 1: Register JDBC driver
	      DbUtils.loadDriver(RESTConstants.JDBC_DRIVER);

	      //Step 2: Open a connection
	      System.out.println("Connecting to database...");
	  
		  conn = DriverManager.getConnection(RESTConstants.DB_URL, RESTConstants.USER, RESTConstants.PASS);
	  
		  int insertedRecords = queryRunner.update(conn, 
				            "INSERT INTO storico_segnalazioni(descrizione)  VALUES (?)",
				             string);
						
		 DbUtils.close(conn);
		
	     
		 return insertedRecords > 0;
	}
	
	public static long insertInizioIrrigazione() throws SQLException {
		  Connection conn = null;
	      QueryRunner queryRunner = new QueryRunner();
	      
	      //Step 1: Register JDBC driver
	      DbUtils.loadDriver(RESTConstants.JDBC_DRIVER);

	      //Step 2: Open a connection
	      System.out.println("Connecting to database...");
	  
		  conn = DriverManager.getConnection(RESTConstants.DB_URL, RESTConstants.USER, RESTConstants.PASS);
		  
		  Long userId = queryRunner.insert(conn,"INSERT INTO storico_irrigazioni(litri_portata) values (?)",
				  							new ScalarHandler<Long>(),0);
		 /* int insertedRecords = queryRunner.update(conn, 
				            "INSERT INTO storico_irrigazioni(litri_portata)  VALUES ()",
				             portata);*/
		 DbUtils.close(conn);
		
	     
		 return userId;
	}
	
	public static boolean updateEndIrrigazione(long id, float totaleErogato) throws SQLException {
		  Connection conn = null;
	      QueryRunner queryRunner = new QueryRunner();
	      
	      //Step 1: Register JDBC driver
	      DbUtils.loadDriver(RESTConstants.JDBC_DRIVER);

	      //Step 2: Open a connection
	      System.out.println("Connecting to database...");
	  
		  conn = DriverManager.getConnection(RESTConstants.DB_URL, RESTConstants.USER, RESTConstants.PASS);
	  
		  int updatedRecords = queryRunner.update(conn, 
				            "UPDATE storico_irrigazioni SET litri_portata = ?,timestamp_fine=NOW() WHERE id = ?",
				             totaleErogato,id);
		 DbUtils.close(conn);
		
	     
		 return updatedRecords > 0;
	}
	
	public static String generateTimeoutErrorMessage(float totaleErogato, float valoreUmidita) {
		return "Non è stato possibile completare l' irrigazione. Totale erogato in " +
				Constants.TMAX_EROGAZIONE+" secondi: " + totaleErogato + " Umidità attuale: "+valoreUmidita;
	}
	
}
