package com.universita.serverJavaSmartGreenHouseREST;

public class RESTConstants {

	public final static String HUMIDITY_VALUE = "HumidityValue";
	
	public final static String MODE = "Mode";
	public final static String EROGATED_LITRES = "ErogatedLitres";
	
	public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	public static final String DB_URL = "jdbc:mysql://localhost:3306/progetto3iot";
	public static final String USER = "progetto3iot";
	public static final String PASS = "progetto3iot";
	public static final int LOCALHOST_PORT = 8080;
	public static final int OK_CODE = 200;
	public static final int INTERNAL_ERROR_CODE = 500;
	

}
