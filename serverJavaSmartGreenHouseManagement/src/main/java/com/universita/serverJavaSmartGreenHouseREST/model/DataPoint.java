package com.universita.serverJavaSmartGreenHouseREST.model;

public class DataPoint {
	private float value;
	
	public DataPoint(float value) {
		this.value = value;
	}
	
	public float getValue() {
		return value;
	}

}
