package com.universita.serverJavaSmartGreenHouseREST.model;

public class SerraInfo {

	private String modalita;
	private float umidita;
	private float quantitaErogata;
	
	public SerraInfo(final String modalita, float umidita, float quantitaErogata) {
		
		this.modalita = modalita;
		this.umidita = umidita;
		this.quantitaErogata = quantitaErogata;
		
	}

	public String getModalita() {
		return modalita;
	}

	public float getUmidita() {
		return umidita;
	}

	public float getQuantitaErogata() {
		return quantitaErogata;
	}
}
