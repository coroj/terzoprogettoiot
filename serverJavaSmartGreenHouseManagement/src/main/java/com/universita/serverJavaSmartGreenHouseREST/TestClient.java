package com.universita.serverJavaSmartGreenHouseREST;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

public class TestClient extends AbstractVerticle {
	
	public static void main(String[] args) {		
	
	//	String host = "496ccad1.ngrok.io";
	//	int port = 80;

		String host = "localhost";
		int port = 8080;
		
		Vertx vertx = Vertx.vertx();
		HttpClient httpClient = vertx.createHttpClient();
		WebClient client = WebClient.wrap(httpClient);
		
		/*il codice di esp dovrà inviare una richesta post tipo questa al dataservice */
		JsonObject itemJson = new JsonObject().put(
				RESTConstants.HUMIDITY_VALUE, 0.4);
		client.post(port,host,"/api/data")
			  .putHeader("content-type", "application/json")
			  .sendJsonObject(itemJson,response ->{
				  if(response.succeeded()) {
					  System.out.println("Received response with status code " + response.result().statusCode());
					  System.out.println("Received response with status message " + response.result().statusMessage());
				  }
			  	});
		
	}
	
}
