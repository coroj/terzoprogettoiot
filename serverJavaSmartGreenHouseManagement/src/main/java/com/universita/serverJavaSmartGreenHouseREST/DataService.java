package com.universita.serverJavaSmartGreenHouseREST;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

import java.util.LinkedList;

import com.universita.serverJavaSmartGreenHouseManagement.patternObserver.Observable;
import com.universita.serverJavaSmartGreenHouseREST.model.DataPoint;
import com.universita.serverJavaSmartGreenHouseManagement.common.Observer;
import com.universita.serverJavaSmartGreenHouseManagement.ServerController;
import com.universita.serverJavaSmartGreenHouseManagement.common.Event;
import com.universita.serverJavaSmartGreenHouseManagement.events.HumidityESPEvent;
import com.universita.serverJavaSmartGreenHouseManagement.events.RESTInfoRequest;
import com.universita.serverJavaSmartGreenHouseManagement.events.RESTInfoResponse;

import java.sql.*;


/*
 * Data Service as a vertx event-loop 
 */
public class DataService extends AbstractVerticle implements Observable,Observer{

	private static DataService dataService = null;
	
	private int port;
	
	private LinkedList<Observer> observers;
	private LinkedList<DataPoint> values;
	
	private RoutingContext restRoutingContext;
	
	private DataService(int port) {
		
		observers = new LinkedList<>();
		values = new LinkedList<DataPoint>();
		this.port = port;

	}

	public static DataService getDataService() {
		
		if(dataService == null) {
			Vertx vertx = Vertx.vertx();
			dataService = new DataService(RESTConstants.LOCALHOST_PORT);
			vertx.deployVerticle(dataService);
			
			ServerController.getServerController().addObserver(dataService); // per ricevere le info dell' umidita dal server
			
		}
		
		return dataService;
		
	}
	
	@Override
	public void start() {		
		Router router = Router.router(vertx);
		router.route().handler(BodyHandler.create());
		router.post("/api/umidita").handler(this::handleAddNewData);
		router.get("/api/umidita").handler(this::handleGetData);	
		router.get("/api/serraInfo").handler(this::handleGetSerraInfo);	
		vertx
			.createHttpServer()
			.requestHandler(router)
			.listen(port);

		log("Service ready.");
	}
	
	private void handleAddNewData(RoutingContext routingContext) {
		HttpServerResponse response = routingContext.response();
		// log("new msg "+routingContext.getBodyAsString());
		JsonObject res = routingContext.getBodyAsJson();
		if (res == null) {
			sendError(400, response);
		} else {
			float value = res.getFloat(RESTConstants.HUMIDITY_VALUE);
			//String place = res.getString("place");
			//long time = System.currentTimeMillis();
			DataPoint dp = new DataPoint(value);
			values.add(dp);
			try {
				
				Utils.insertHumidityValue(dp);
				response.setStatusCode(RESTConstants.OK_CODE);
				response.setStatusMessage("OK");
				this.notifyEventToObservers(new HumidityESPEvent(dp.getValue()));
				
			} catch (SQLException e) {
				response.setStatusCode(RESTConstants.INTERNAL_ERROR_CODE);
				response.setStatusMessage(e.getMessage());
			}
			
			response.end();
			
		}
	}
	
	private void handleGetData(RoutingContext routingContext) {
		JsonArray arr = new JsonArray();
		for (DataPoint p: values) {
			JsonObject data = new JsonObject();
			data.put(RESTConstants.HUMIDITY_VALUE, p.getValue());
			arr.add(data);
		}
		routingContext.response()
			.setStatusCode(RESTConstants.OK_CODE)
			.putHeader("content-type", "application/json")
			.end(arr.encodePrettily());
	}
	
	private void handleGetSerraInfo(RoutingContext routingContext) {
		
		if(restRoutingContext == null) {
			
			restRoutingContext = routingContext;
			
			this.notifyEventToObservers(new RESTInfoRequest());
			
		}
		
	}
	
	private void sendError(int statusCode, HttpServerResponse response) {
		response.setStatusCode(statusCode).end();
	}

	private void log(String msg) {
		System.out.println("[DATA SERVICE] "+msg);
	}
	
	////Observable code rest -> server java
	
	@Override
	public void notifyEventToObservers(Event ev){
		synchronized (observers){
			for (Observer obs: observers){
				obs.notifyEvent(ev);
			}
		}
	}
	
	@Override
	public void addObserver(Observer obs){
		synchronized (observers){
			observers.add(obs);
		}
	}

	@Override
	public void removeObserver(Observer obs){
		synchronized (observers){
			observers.remove(obs);
		}
	}

	
	
	////// observer code server java -> rest
	@Override
	public boolean notifyEvent(Event ev) {
		if(ev instanceof RESTInfoResponse) {
			
			if(restRoutingContext != null) {
				RESTInfoResponse rif = (RESTInfoResponse)ev;
		
				JsonObject data = new JsonObject();
				data.put(RESTConstants.HUMIDITY_VALUE, rif.getSerraInfo().getUmidita());
				data.put(RESTConstants.EROGATED_LITRES, rif.getSerraInfo().getQuantitaErogata());
				data.put(RESTConstants.MODE, rif.getSerraInfo().getModalita());
				
			
				restRoutingContext.response()
				.setStatusCode(RESTConstants.OK_CODE)
				.putHeader("content-type", "application/json")
				.end(data.encode());
				
				restRoutingContext = null;
				
				return true;
				
			}
			
			return false;
			
		}
		
		return false;
	}
	
}